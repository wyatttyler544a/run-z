## Styleguide

The Markdown files follow this styleguide: https://cirosantilli.com/markdown-style-guide/

## Linting

This Remark Markdown linting preset is used to check problems in the Markdown files.
https://github.com/remarkjs/remark-lint/tree/main/packages/remark-preset-lint-markdown-style-guide

## Formatting

The npm script `format` executes [Prettier](https://prettier.io) to format all Markdown files.

## Comments

<https://cirosantilli.com/markdown-style-guide/> is chosen only because it is implemented in
[the Markdown linting preset](https://github.com/remarkjs/remark-lint/tree/main/packages/remark-preset-lint-markdown-style-guide)
which is the most complete solution for writing maintainable Markdown documentations.

I and any of my collaborators in no way support any opinion, the author of this styleguide may hold.
